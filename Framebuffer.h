#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include "Device.h"
#include "Swapchain.h" 
#include "RenderPass.h"

#include <vulkan/vulkan.h>


struct vkm_DepthStencilAttachment
{
    VkImage        vk_depthImage;
    VkImageView    vk_depthImageView;
    VkDeviceMemory vk_deviceMemory;
};

struct vkm_FramebufferManager
{
    VkFramebuffer vk_framebuffers[COUNT_SWAPCHAIN_IMAGES];
    struct vkm_DepthStencilAttachment vkm_depthStencilAttachment;
};

void createDepthImageView(struct vkm_Device* vkm_device, struct vkm_Swapchain* vkm_swapchain, struct vkm_RenderPass* vkm_renderPass, struct vkm_FramebufferManager* vkm_framebufferManager);

void createFramebuffers(struct vkm_Device* vkm_device, struct vkm_Swapchain* vkm_swapchain, struct vkm_RenderPass* vkm_renderPass, struct vkm_FramebufferManager* vkm_framebufferManager);

#endif // FRAMEBUFFER_H