#ifndef INSTANCE_H
#define INSTANCE_H

#include <vulkan/vulkan.h>

struct vkm_Instance
{
    VkInstance vk_instance;

    //! Needs to be freed
    uint32_t count_extensions;
    const char** extensions;

    uint32_t count_layers;
    const char** layers;
};

void createInstance(struct vkm_Instance* vkm_instance, const char** instanceExtensions, const uint32_t count_instanceExtensions, const char** instanceLayers, const uint32_t count_instanceLayers);

#endif // INSTANCE_H