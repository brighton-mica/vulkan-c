#include "Instance.h"

#include <stdlib.h>
#include <stdio.h>

void createInstance(struct vkm_Instance* vkm_instance, char const** instanceExtensions, const uint32_t count_instanceExtensions, const char** instanceLayers, const uint32_t count_instanceLayers)
{
    VkApplicationInfo ci_app;
    ci_app.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    ci_app.pNext = NULL;
    ci_app.pApplicationName = "Triangle";
    ci_app.applicationVersion = VK_MAKE_VERSION(0, 0, 0);
    ci_app.pEngineName = "Mica";
    ci_app.engineVersion = VK_MAKE_VERSION(0, 0, 0);
    ci_app.apiVersion = VK_API_VERSION_1_2;

    VkInstanceCreateInfo ci_instance;
    ci_instance.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    ci_instance.pNext = NULL;
    ci_instance.flags = 0x0;
    ci_instance.pApplicationInfo = &ci_app;
    ci_instance.enabledExtensionCount = count_instanceExtensions;
    ci_instance.ppEnabledExtensionNames = instanceExtensions;
    ci_instance.enabledLayerCount = count_instanceLayers;
    ci_instance.ppEnabledLayerNames = instanceLayers;

    VkResult result = vkCreateInstance(&ci_instance, NULL, &vkm_instance->vk_instance);
    if (result != VK_SUCCESS)
    {
        perror("Failed to create a Vulkan Instance.");
        exit(EXIT_FAILURE);
    }
}