#include "RenderPass.h"

#include <stdio.h>

void createRenderPass(struct vkm_Device* vkm_device, struct vkm_Swapchain* vk_swapchain, struct vkm_RenderPass* vkm_renderPass)
{
    VkAttachmentDescription attachments[2] = {};

    // Color
    attachments[0].flags = 0x0;
    attachments[0].format = vk_swapchain->ci_swapchain.imageFormat;
    attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
    attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    attachments[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    // Depth
    attachments[1].flags = 0x0;
    attachments[1].format = VK_FORMAT_D16_UNORM;
    attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
    attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachments[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    /*
     * VkAttachmentReference {
     *      .attachment - integer value corresponding to index in VkRenderPassCreateInfo.pAttachments
     *      .layout - the layout the attachment uses during the subpass
     * }
    */

    VkAttachmentReference colorReference;
    colorReference.attachment = 0;
    colorReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference depthReference;
    depthReference.attachment = 1;
    depthReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    /*
     * VkSubpassDescription {
     *      .pipelineBindPoint - Graphics/Compute
     *      .inputAttachments - attachments the subpass reads from
     *      .colorAttachments - attachments the subpass writes to
     *      .depthStencilAttachments - depth/stencil attachment this subpass writes to
     * } 
    */

    VkSubpassDescription subpass;
    subpass.flags = 0x0;
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.inputAttachmentCount = 0;
    subpass.pInputAttachments = NULL;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorReference;
    subpass.pResolveAttachments = NULL;
    subpass.pDepthStencilAttachment = &depthReference;
    subpass.preserveAttachmentCount = 0;
    subpass.pPreserveAttachments = NULL;

   /*
    * VkSubpassDependency {
    *       .srcSubpass - subpass index of the first subpass in the dependency (or EXTERNAL)
    *       .dstSubpass - subpass index of the second subpass in the dependency (or EXTERNAL)
    *       .srcStageMask - the first sync scope only includes execution of these stages
    *       .dstStageMask - the second sync scope only includes execution of these stages
    *       .srcAccessMask - memory access available to the srcStageMask
    *       .dstAccessMask - memory access available to the dstStageMask
    *       .dependencyFlags - by Region means Frambuffer local
    * } 
   */

	VkSubpassDependency dependencies[2];

	// First dependency at the start of the renderpass
	// Does the transition from final to initial layout
	dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;                             // Producer of the dependency
	dependencies[0].dstSubpass = 0;                                               // Consumer is our single subpass that will wait for the execution dependency
	dependencies[0].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT; // Match our pWaitDstStageMask when we vkQueueSubmit
	dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT; // is a loadOp stage for color attachments
	dependencies[0].srcAccessMask = 0;                                            // semaphore wait already does memory dependency for us
	dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;         // is a loadOp CLEAR access mask for color attachments
	dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	// Second dependency at the end the renderpass
	// Does the transition from the initial to the final layout
	// Technically this is the same as the implicit subpass dependency, but we are gonna state it explicitly here
	dependencies[1].srcSubpass = 0;                                               // Producer of the dependency is our single subpass
	dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;                             // Consumer are all commands outside of the renderpass
	dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT; // is a storeOp stage for color attachments
	dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;          // Do not block any subsequent work
	dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;         // is a storeOp `STORE` access mask for color attachments
	dependencies[1].dstAccessMask = 0;
	dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

    /*
     * VkRenderPassCreateInfo {
     *      .attachments - the attachments used in this render pass
     *      .subpasses - the subpasses used in this render pass
     *      .dependencies - the subpass dependencies for this render pass
     * }
    */

    vkm_renderPass->ci_renderPass.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
    vkm_renderPass->ci_renderPass.pNext = NULL;
    vkm_renderPass->ci_renderPass.flags = 0x0;
    vkm_renderPass->ci_renderPass.attachmentCount = 2;
    vkm_renderPass->ci_renderPass.pAttachments = attachments;
    vkm_renderPass->ci_renderPass.subpassCount = 1;
    vkm_renderPass->ci_renderPass.pSubpasses = &subpass;
    vkm_renderPass->ci_renderPass.dependencyCount = 2;
    vkm_renderPass->ci_renderPass.pDependencies = dependencies;

    VkResult result = vkCreateRenderPass(vkm_device->vk_device, &vkm_renderPass->ci_renderPass, NULL, &vkm_renderPass->vk_renderPass);
    if (result != VK_SUCCESS)
    {
        perror("Failed to create RenderPass!\n");
    }
}
