#include "Pipeline.h"

#include <stdlib.h>
#include <stdio.h>


VkShaderModule createShaderModule(struct vkm_Device* vkm_device, const char* filename)
{
    FILE* f = fopen(filename, "r");
    if (f == NULL)
    {
        printf("Failed to open file %s!\n", filename);
        exit(EXIT_FAILURE);
    }

    fseek(f, 0, SEEK_END);
    const size_t nbytes_fileSize = (size_t)ftell(f);
    rewind(f);

    uint32_t* buffer = (uint32_t*)malloc(nbytes_fileSize);
    fread(buffer, nbytes_fileSize, 1, f);
    fclose(f);

    VkShaderModuleCreateInfo ci_shaderModule;
    ci_shaderModule.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    ci_shaderModule.pNext = NULL;
    ci_shaderModule.flags = 0x0;
    ci_shaderModule.codeSize = nbytes_fileSize;
    ci_shaderModule.pCode = buffer;

    VkShaderModule vk_shaderModule;
    VkResult result = vkCreateShaderModule(vkm_device->vk_device, &ci_shaderModule, NULL, &vk_shaderModule);
    if (result != VK_SUCCESS)
    {
        printf("Failed to create shader module for %s!\n", filename);
        exit(EXIT_FAILURE);
    }

    free(buffer);

    return vk_shaderModule;
}


void createDefaultGraphicsPipeline(struct vkm_Device* vkm_device, struct vkm_Swapchain* vk_swapchain, struct vkm_RenderPass* vkm_renderPass, struct vkm_GraphicsPipeline* vkm_graphicsPipeline)
{
    //*** Shader Stage

    VkPipelineShaderStageCreateInfo ci_pipelineShaderStages[2];
    ci_pipelineShaderStages[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    ci_pipelineShaderStages[0].pNext = NULL;
    ci_pipelineShaderStages[0].flags = 0x0;
    ci_pipelineShaderStages[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
    ci_pipelineShaderStages[0].module = createShaderModule(vkm_device, "../shaders/default-vert.spv");
    ci_pipelineShaderStages[0].pName = "main";
    ci_pipelineShaderStages[0].pSpecializationInfo = NULL;

    ci_pipelineShaderStages[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    ci_pipelineShaderStages[1].pNext = NULL;
    ci_pipelineShaderStages[1].flags = 0x0;
    ci_pipelineShaderStages[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    ci_pipelineShaderStages[1].module = createShaderModule(vkm_device, "../shaders/default-frag.spv");
    ci_pipelineShaderStages[1].pName = "main";
    ci_pipelineShaderStages[1].pSpecializationInfo = NULL;

    //*** Input State

    VkVertexInputBindingDescription vk_inputBindingDescription;
    vk_inputBindingDescription.binding = 0;
    vk_inputBindingDescription.stride = sizeof(float) * 6;
    vk_inputBindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    VkVertexInputAttributeDescription vk_inputAttributeDescriptions[2] = { 0 };

    // Pos (vec3)
    vk_inputAttributeDescriptions[0].location = 0;
    vk_inputAttributeDescriptions[0].binding = 0;
    vk_inputAttributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
    vk_inputAttributeDescriptions[0].offset = 0;

    // Norm (vec3)
    vk_inputAttributeDescriptions[1].location = 1;
    vk_inputAttributeDescriptions[1].binding = 0;
    vk_inputAttributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
    vk_inputAttributeDescriptions[1].offset = sizeof(float) * 3;

    VkPipelineVertexInputStateCreateInfo ci_pipelineVertexInputState;
    ci_pipelineVertexInputState.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    ci_pipelineVertexInputState.pNext = NULL;
    ci_pipelineVertexInputState.flags = 0x0;
    ci_pipelineVertexInputState.vertexBindingDescriptionCount = 1;
    ci_pipelineVertexInputState.pVertexBindingDescriptions = &vk_inputBindingDescription;
    ci_pipelineVertexInputState.vertexAttributeDescriptionCount = 2;
    ci_pipelineVertexInputState.pVertexAttributeDescriptions = vk_inputAttributeDescriptions;

    //*** Input Assembly

    VkPipelineInputAssemblyStateCreateInfo ci_pipelineInputAssemblyState;
    ci_pipelineInputAssemblyState.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    ci_pipelineInputAssemblyState.pNext = NULL;
    ci_pipelineInputAssemblyState.flags = 0x0;
    ci_pipelineInputAssemblyState.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    ci_pipelineInputAssemblyState.primitiveRestartEnable = VK_FALSE;

    //*** Tessellation State

    VkPipelineTessellationStateCreateInfo ci_pipelineTesselationState;
    ci_pipelineTesselationState.sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
    ci_pipelineTesselationState.pNext = NULL;
    ci_pipelineTesselationState.flags = 0x0;
    ci_pipelineTesselationState.patchControlPoints = 0;

    //*** Viewport State

    VkViewport vk_viewport;
    vk_viewport.x = 0;
    vk_viewport.y = 0;
    vk_viewport.width = (float)vk_swapchain->ci_swapchain.imageExtent.width;
    vk_viewport.height = (float)vk_swapchain->ci_swapchain.imageExtent.height;
    vk_viewport.minDepth = 0.0f;
    vk_viewport.maxDepth = 1.0f;

    VkRect2D vk_rect2DScissor;
    vk_rect2DScissor.offset.x = 0;
    vk_rect2DScissor.offset.y = 0;
    vk_rect2DScissor.extent = vk_swapchain->ci_swapchain.imageExtent;

    VkPipelineViewportStateCreateInfo ci_pipelineViewportState;
    ci_pipelineViewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    ci_pipelineViewportState.pNext = NULL;
    ci_pipelineViewportState.flags = 0x0;
    ci_pipelineViewportState.viewportCount = 1;
    ci_pipelineViewportState.pViewports = &vk_viewport;
    ci_pipelineViewportState.scissorCount = 1;
    ci_pipelineViewportState.pScissors = &vk_rect2DScissor;

    //*** Rasterization State

    VkPipelineRasterizationStateCreateInfo ci_pipelineRasterizationState;
    ci_pipelineRasterizationState.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    ci_pipelineRasterizationState.pNext = NULL;
    ci_pipelineRasterizationState.flags = 0x0;
    ci_pipelineRasterizationState.depthClampEnable = VK_FALSE;
    ci_pipelineRasterizationState.rasterizerDiscardEnable = VK_FALSE;
    ci_pipelineRasterizationState.polygonMode = VK_POLYGON_MODE_FILL;
    ci_pipelineRasterizationState.cullMode = VK_CULL_MODE_NONE;
    ci_pipelineRasterizationState.frontFace = VK_FRONT_FACE_CLOCKWISE;
    ci_pipelineRasterizationState.depthBiasEnable = VK_FALSE;
    ci_pipelineRasterizationState.depthBiasConstantFactor = 0.f;
    ci_pipelineRasterizationState.depthBiasClamp = 0.f;
    ci_pipelineRasterizationState.depthBiasSlopeFactor = 0.f;
    ci_pipelineRasterizationState.lineWidth = 1.f;

    //*** MultiSample State

    VkPipelineMultisampleStateCreateInfo ci_pipelineMultisampleState;
    ci_pipelineMultisampleState.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    ci_pipelineMultisampleState.pNext = NULL;
    ci_pipelineMultisampleState.flags = 0x0;
    ci_pipelineMultisampleState.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    ci_pipelineMultisampleState.sampleShadingEnable = VK_FALSE;
    ci_pipelineMultisampleState.minSampleShading = 0;
    ci_pipelineMultisampleState.pSampleMask = NULL;
    ci_pipelineMultisampleState.alphaToCoverageEnable = VK_FALSE;
    ci_pipelineMultisampleState.alphaToOneEnable = VK_FALSE;

    //*** DepthStencil State

    VkStencilOpState vk_stencilOpStateFront;
    vk_stencilOpStateFront.failOp = VK_STENCIL_OP_KEEP;
    vk_stencilOpStateFront.passOp = VK_STENCIL_OP_KEEP;
    vk_stencilOpStateFront.depthFailOp = VK_STENCIL_OP_KEEP;
    vk_stencilOpStateFront.compareOp = VK_COMPARE_OP_NEVER;
    vk_stencilOpStateFront.compareMask = 0x0;
    vk_stencilOpStateFront.writeMask = 0x0;
    vk_stencilOpStateFront.reference = 0x0;

    VkStencilOpState vk_stencilOpStateBack;
    vk_stencilOpStateBack.failOp = VK_STENCIL_OP_KEEP;
    vk_stencilOpStateBack.passOp = VK_STENCIL_OP_KEEP;
    vk_stencilOpStateBack.depthFailOp = VK_STENCIL_OP_KEEP;
    vk_stencilOpStateBack.compareOp = VK_COMPARE_OP_NEVER;
    vk_stencilOpStateBack.compareMask = 0x0;
    vk_stencilOpStateBack.writeMask = 0x0;
    vk_stencilOpStateBack.reference = 0x0;

    VkPipelineDepthStencilStateCreateInfo ci_pipelineDepthStencilState;
    ci_pipelineDepthStencilState.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    ci_pipelineDepthStencilState.pNext = NULL;
    ci_pipelineDepthStencilState.flags = 0x0;
    ci_pipelineDepthStencilState.depthTestEnable = VK_FALSE;
    ci_pipelineDepthStencilState.depthWriteEnable = VK_FALSE;
    ci_pipelineDepthStencilState.depthCompareOp = VK_COMPARE_OP_LESS;
    ci_pipelineDepthStencilState.depthBoundsTestEnable = VK_FALSE;
    ci_pipelineDepthStencilState.front = vk_stencilOpStateFront;
    ci_pipelineDepthStencilState.back = vk_stencilOpStateBack;
    ci_pipelineDepthStencilState.minDepthBounds = 0.f;
    ci_pipelineDepthStencilState.minDepthBounds = 1.f;
    ci_pipelineDepthStencilState.stencilTestEnable = VK_FALSE;

    //*** Blend State

    // Base Color Attachment
    VkPipelineColorBlendAttachmentState vk_pipelineColorBlendAttachmentState;
    vk_pipelineColorBlendAttachmentState.blendEnable = VK_FALSE;
    vk_pipelineColorBlendAttachmentState.srcColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    vk_pipelineColorBlendAttachmentState.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    vk_pipelineColorBlendAttachmentState.colorBlendOp = VK_BLEND_OP_ADD;
    vk_pipelineColorBlendAttachmentState.srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    vk_pipelineColorBlendAttachmentState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    vk_pipelineColorBlendAttachmentState.alphaBlendOp = VK_BLEND_OP_ADD;
    vk_pipelineColorBlendAttachmentState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

    VkPipelineColorBlendStateCreateInfo ci_pipelineColorBlendState;
    ci_pipelineColorBlendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    ci_pipelineColorBlendState.pNext = NULL;
    ci_pipelineColorBlendState.flags = 0x0;
    ci_pipelineColorBlendState.logicOpEnable = VK_FALSE;
    ci_pipelineColorBlendState.logicOp = VK_LOGIC_OP_COPY;
    ci_pipelineColorBlendState.attachmentCount = 1;
    ci_pipelineColorBlendState.pAttachments = &vk_pipelineColorBlendAttachmentState;
    ci_pipelineColorBlendState.blendConstants[0] = 0;
    ci_pipelineColorBlendState.blendConstants[1] = 0;
    ci_pipelineColorBlendState.blendConstants[2] = 0;
    ci_pipelineColorBlendState.blendConstants[3] = 0;

    //*** Dynamic State

    VkPipelineDynamicStateCreateInfo ci_pipelineDynamicState;
    ci_pipelineDynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    ci_pipelineDynamicState.pNext = NULL;
    ci_pipelineDynamicState.flags = 0x0;
    ci_pipelineDynamicState.dynamicStateCount = 0;
    ci_pipelineDynamicState.pDynamicStates = NULL; // TODO - set this to viewportr and scissor for window resizing

    //*** Layout

    VkPipelineLayoutCreateInfo ci_pipelineLayout;
    ci_pipelineLayout.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    ci_pipelineLayout.pNext = NULL;
    ci_pipelineLayout.flags = 0x0;
    ci_pipelineLayout.setLayoutCount = 0;
    ci_pipelineLayout.pSetLayouts = NULL;
    ci_pipelineLayout.pushConstantRangeCount = 0;
    ci_pipelineLayout.pPushConstantRanges = NULL;

    VkResult result = vkCreatePipelineLayout(vkm_device->vk_device, &ci_pipelineLayout, NULL, &vkm_graphicsPipeline->vk_pipelineLayout);
    if (result != VK_SUCCESS)
    {
        perror("Failed to create default pipeline layout!\n");
        exit(EXIT_FAILURE);
    }

    //*** Graphics Pipeline Creation

    VkGraphicsPipelineCreateInfo ci_graphicsPipeline;
    ci_graphicsPipeline.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    ci_graphicsPipeline.pNext = NULL;
    ci_graphicsPipeline.flags = 0x0;
    ci_graphicsPipeline.stageCount = 2;
    ci_graphicsPipeline.pStages = ci_pipelineShaderStages;
    ci_graphicsPipeline.pVertexInputState = &ci_pipelineVertexInputState;
    ci_graphicsPipeline.pInputAssemblyState = &ci_pipelineInputAssemblyState;
    ci_graphicsPipeline.pTessellationState = &ci_pipelineTesselationState;
    ci_graphicsPipeline.pViewportState = &ci_pipelineViewportState;
    ci_graphicsPipeline.pRasterizationState = &ci_pipelineRasterizationState;
    ci_graphicsPipeline.pMultisampleState = &ci_pipelineMultisampleState;
    ci_graphicsPipeline.pDepthStencilState = &ci_pipelineDepthStencilState;
    ci_graphicsPipeline.pColorBlendState = &ci_pipelineColorBlendState;
    ci_graphicsPipeline.pDynamicState = &ci_pipelineDynamicState;
    ci_graphicsPipeline.layout = vkm_graphicsPipeline->vk_pipelineLayout;
    ci_graphicsPipeline.renderPass = vkm_renderPass->vk_renderPass;
    ci_graphicsPipeline.subpass = 0;
    ci_graphicsPipeline.basePipelineHandle = VK_NULL_HANDLE;
    ci_graphicsPipeline.basePipelineIndex = 0;

    result = vkCreateGraphicsPipelines(vkm_device->vk_device, VK_NULL_HANDLE, 1, &ci_graphicsPipeline, NULL, &vkm_graphicsPipeline->vk_pipeline);
    if (result != VK_SUCCESS)
    {
        perror("Failed to create default graphics pipeline!\n");
        exit(EXIT_FAILURE);
    }

    vkDestroyShaderModule(vkm_device->vk_device, ci_pipelineShaderStages[0].module, NULL);
    vkDestroyShaderModule(vkm_device->vk_device, ci_pipelineShaderStages[1].module, NULL);
}
