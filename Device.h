#ifndef PHYSICAL_DEVICE_H
#define PHYSICAL_DEVICE_H

#include <vulkan/vulkan.h>

struct vkm_Instance;

struct vkm_Queue
{
    uint32_t index;
    VkQueue vk_handle;
};

struct vkm_Device
{
    VkDevice vk_device;
    VkPhysicalDevice vk_physicalDevice;
    VkPhysicalDeviceProperties vk_physicalDeviceProperties;
    VkPhysicalDeviceMemoryProperties vk_physicalDeviceMemoryProperties;

    struct vkm_Queue queue_graphics;
    struct vkm_Queue queue_present;
};

//*** Creation/Destruction

void selectPhysicalDevice(struct vkm_Device* vkm_device, struct vkm_Instance* vkm_instance);
void selectQueueFamilies(struct vkm_Device* device, const VkSurfaceKHR requestedSurface);
void createLogicalDevice(struct vkm_Device* vkm_device, const char** requestedExtensions, const uint32_t count_requestedExtensions);

void destroyDevice(struct vkm_Device* vkm_device);

//*** Utility

uint32_t getMemoryTypeIndex(const struct vkm_Device* vkm_device, const uint32_t typeBits, const VkMemoryPropertyFlags properties);




#endif // PHYSICAL_DEVICE_H