#include "Swapchain.h"

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>

void createSwapchain(
    const struct vkm_Device* vkm_device,
    struct vkm_Swapchain* vk_swapchain,
    const uint32_t requestedImageCount,
    const VkSurfaceKHR requestedSurface, 
    const VkFormat requestedFormat,
    const VkExtent2D requestedImageExtent)
{
    //*** sType, pNext, flags, Surface
    vk_swapchain->ci_swapchain.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    vk_swapchain->ci_swapchain.pNext = NULL;
    vk_swapchain->ci_swapchain.flags = 0;
    vk_swapchain->ci_swapchain.surface = requestedSurface;

    VkSurfaceCapabilitiesKHR vk_surfaceCapabilities;
    VkResult result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(vkm_device->vk_physicalDevice, requestedSurface, &vk_surfaceCapabilities);
    if (result != VK_SUCCESS)
    {
        perror("Failed to query Physical Device Surface capabilities!\n");
        exit(EXIT_FAILURE);
    }

    //*** Image Count
    {
        assert(requestedImageCount > 0 && "Invalid requested image count for swapchain!\n");

        // If the minImageCount is 0, then there is not a limit on the number of images the swapchain
        // can support (ignoring memory constraints). See the Vulkan Spec for more information.
        if (vk_surfaceCapabilities.maxImageCount == 0)
        {
            if (requestedImageCount >= vk_surfaceCapabilities.minImageCount)
            {
                vk_swapchain->ci_swapchain.minImageCount = requestedImageCount;
            }
            else
            {
                printf("Failed to create Swapchain. The requested number of images %u does not meet the minimum requirement of %u.\n", requestedImageCount, vk_surfaceCapabilities.minImageCount);
                exit(EXIT_FAILURE);
            }
        }
        else if (requestedImageCount >= vk_surfaceCapabilities.minImageCount &&
                 requestedImageCount <= vk_surfaceCapabilities.maxImageCount)
        {
            vk_swapchain->ci_swapchain.minImageCount = requestedImageCount;
        }
        else
        {
            printf("The number of requested Swapchain images %u is not supported. Min: %u\t Max: %u\n", requestedImageCount, vk_surfaceCapabilities.minImageCount, vk_surfaceCapabilities.maxImageCount);
            exit(EXIT_FAILURE);
        }
    }

    //*** Image Format
    {
        uint32_t count_supportedSurfaceFormats = 0;
        result = vkGetPhysicalDeviceSurfaceFormatsKHR(vkm_device->vk_physicalDevice, requestedSurface, &count_supportedSurfaceFormats, NULL);
        VkSurfaceFormatKHR* vk_supportedSurfaceFormats = (VkSurfaceFormatKHR*)malloc(sizeof(VkSurfaceFormatKHR)*count_supportedSurfaceFormats); 
        result = vkGetPhysicalDeviceSurfaceFormatsKHR(vkm_device->vk_physicalDevice, requestedSurface, &count_supportedSurfaceFormats, vk_supportedSurfaceFormats);
        if (result != VK_SUCCESS)
        {
            perror("Failed to query for supported surface formats during swapchain creation!\n");
            exit(EXIT_FAILURE);
        }

        bool requestedFormatFound = false;
        for (uint32_t i = 0; i < count_supportedSurfaceFormats; ++i)
        {
            if (vk_supportedSurfaceFormats[i].format == requestedFormat)
            {
                vk_swapchain->ci_swapchain.imageFormat = vk_supportedSurfaceFormats[i].format;
                vk_swapchain->ci_swapchain.imageColorSpace = vk_supportedSurfaceFormats[i].colorSpace;
                requestedFormatFound = true;
                break;
            }
        }

        if (!requestedFormatFound)
        {
            vk_swapchain->ci_swapchain.imageFormat = vk_supportedSurfaceFormats[0].format;
            vk_swapchain->ci_swapchain.imageColorSpace = vk_supportedSurfaceFormats[0].colorSpace;
            printf("WARNING - Requested format %u is not avaliable! Defaulting to first avaliable.\n", (unsigned int)requestedFormat);
        }

        free(vk_supportedSurfaceFormats);
    }

    //*** Extent (size)
    {
        // The Vulkan Spec states that if the current width/height is 0xFFFFFFFF, then the surface size
        // will be deteremined by the extent specified in the VkSwapchainCreateInfoKHR.
        if (vk_surfaceCapabilities.currentExtent.width != (uint32_t)-1)
        {
            vk_swapchain->ci_swapchain.imageExtent.width = requestedImageExtent.width;
            vk_swapchain->ci_swapchain.imageExtent.height = requestedImageExtent.height;
        }
        else
        {
            vk_swapchain->ci_swapchain.imageExtent.width = vk_surfaceCapabilities.currentExtent.width;            
            vk_swapchain->ci_swapchain.imageExtent.height = vk_surfaceCapabilities.currentExtent.height;            
        }
    }

    //*** Array Layers, Image Usage, Image Sharing Mode
    vk_swapchain->ci_swapchain.imageArrayLayers = 1;
    vk_swapchain->ci_swapchain.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    vk_swapchain->ci_swapchain.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    vk_swapchain->ci_swapchain.queueFamilyIndexCount = 0;
    vk_swapchain->ci_swapchain.pQueueFamilyIndices = NULL;

    //** Pre Transform
    {
        if (vk_surfaceCapabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
        {
            vk_swapchain->ci_swapchain.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
        }
        else
        {
            vk_swapchain->ci_swapchain.preTransform = vk_surfaceCapabilities.currentTransform;
            printf("WARNING - Swapchain pretransform is not IDENTITIY_BIT_KHR!\n");
        }
    }

    //** Composite Alpha
    {
        // Determine the composite alpha format the application needs.
    	// Find a supported composite alpha format (not all devices support alpha opaque),
        // but we prefer it.
    	// Simply select the first composite alpha format available
        // Used for blending with other windows in the system
    	VkCompositeAlphaFlagBitsKHR compositeAlphaFlags[4] = {
    		VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
    		VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR,
    		VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR,
    		VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR,
    	};
        for (size_t i = 0; i < 4; ++i)
        {
    		if (vk_surfaceCapabilities.supportedCompositeAlpha & compositeAlphaFlags[i]) 
            {
                vk_swapchain->ci_swapchain.compositeAlpha = compositeAlphaFlags[i];
                break;
    		};
        }
    }

    //** Present Mode
    {
        uint32_t count_supportedPresentModes = 0;
        result = vkGetPhysicalDeviceSurfacePresentModesKHR(vkm_device->vk_physicalDevice, requestedSurface, &count_supportedPresentModes, NULL);
        VkPresentModeKHR* vk_supportedPresentModes = (VkPresentModeKHR*)malloc(sizeof(VkPresentModeKHR)*count_supportedPresentModes);
        result = vkGetPhysicalDeviceSurfacePresentModesKHR(vkm_device->vk_physicalDevice, requestedSurface, &count_supportedPresentModes, vk_supportedPresentModes);
        if (result != VK_SUCCESS)
        {
            perror("Failed to query for supported present modes during swapchain creation!\n");
            exit(EXIT_FAILURE);
        }

        // Determine the present mode the application needs.
        // Try to use mailbox, it is the lowest latency non-tearing present mode
        // All devices support FIFO (this mode waits for the vertical blank or v-sync)
        vk_swapchain->ci_swapchain.presentMode = VK_PRESENT_MODE_FIFO_KHR;
        for (uint32_t i = 0; i < count_supportedPresentModes; ++i)
        {
            if (vk_supportedPresentModes[i] == VK_PRESENT_MODE_MAILBOX_KHR)
            {
                vk_swapchain->ci_swapchain.presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
                break;
            }
        }

        free(vk_supportedPresentModes);
    }

    //** Clipped, Old Swapchain
    vk_swapchain->ci_swapchain.clipped = VK_TRUE;
    vk_swapchain->ci_swapchain.oldSwapchain = VK_NULL_HANDLE;

    result = vkCreateSwapchainKHR(vkm_device->vk_device, &vk_swapchain->ci_swapchain, NULL, &vk_swapchain->vk_swapchain);
    if (result != VK_SUCCESS)
    {
        perror("Failed to creat swapchain!\n");
        exit(EXIT_FAILURE);
    }

    //*** Get Images/ImageViews

    uint32_t count_actualSwapchainImages = 0;
    vkGetSwapchainImagesKHR(vkm_device->vk_device, vk_swapchain->vk_swapchain, &count_actualSwapchainImages, NULL);
    assert(count_actualSwapchainImages == COUNT_SWAPCHAIN_IMAGES && "Actual swapchain image count does not equal requested count!\n");
    vkGetSwapchainImagesKHR(vkm_device->vk_device, vk_swapchain->vk_swapchain, &count_actualSwapchainImages, vk_swapchain->vk_images);

    for (size_t i = 0; i < COUNT_SWAPCHAIN_IMAGES; ++i)
    {
        VkImageViewCreateInfo vk_colorAttachmentView;
        vk_colorAttachmentView.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        vk_colorAttachmentView.pNext = NULL;
        vk_colorAttachmentView.flags = 0;
        vk_colorAttachmentView.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        vk_colorAttachmentView.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        vk_colorAttachmentView.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        vk_colorAttachmentView.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        vk_colorAttachmentView.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        vk_colorAttachmentView.subresourceRange.baseMipLevel = 0;
        vk_colorAttachmentView.subresourceRange.levelCount = 1;
        vk_colorAttachmentView.subresourceRange.baseArrayLayer = 0;
        vk_colorAttachmentView.subresourceRange.layerCount = 1;
        vk_colorAttachmentView.viewType = VK_IMAGE_VIEW_TYPE_2D;
        vk_colorAttachmentView.image = vk_swapchain->vk_images[i];
        vk_colorAttachmentView.format = vk_swapchain->ci_swapchain.imageFormat;

        VkResult result = vkCreateImageView(vkm_device->vk_device, &vk_colorAttachmentView, NULL, &vk_swapchain->vk_imageViews[i]);
        if (result != VK_SUCCESS)
        {
            perror("Failed to create swapchain image view!");
            exit(EXIT_FAILURE);
        }
    }
}
