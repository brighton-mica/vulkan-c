#ifndef RENDERPASS_H
#define RENDERPASS_H

#include "RenderPass.h"
#include "Swapchain.h"

#include <vulkan/vulkan.h>

struct vkm_RenderPass
{
    VkRenderPass vk_renderPass;
    VkRenderPassCreateInfo ci_renderPass;
};

void createRenderPass(struct vkm_Device* vkm_device, struct vkm_Swapchain* vk_swapchain, struct vkm_RenderPass* vkm_renderPass);

#endif // RENDERPASS_H