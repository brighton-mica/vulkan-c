#include "Device.h"
#include "Instance.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

void selectPhysicalDevice(struct vkm_Device* vkm_device, struct vkm_Instance* vkm_instance)
{
    // Query avaliable Physical Devices
    uint32_t count_physicalDevices = 0;
    vkEnumeratePhysicalDevices(vkm_instance->vk_instance, &count_physicalDevices, NULL);
    VkPhysicalDevice* physicalDevices = (VkPhysicalDevice*)malloc(sizeof(VkPhysicalDevice)*count_physicalDevices);
    vkEnumeratePhysicalDevices(vkm_instance->vk_instance, &count_physicalDevices, physicalDevices);

    // Select the first Physical Device
    vkm_device->vk_physicalDevice = physicalDevices[0];

    // Store properties for later use
    vkGetPhysicalDeviceProperties(vkm_device->vk_physicalDevice, &vkm_device->vk_physicalDeviceProperties);
    printf("Physical Device: %s\n", vkm_device->vk_physicalDeviceProperties.deviceName);
    vkGetPhysicalDeviceMemoryProperties(vkm_device->vk_physicalDevice, &vkm_device->vk_physicalDeviceMemoryProperties);

    free(physicalDevices);
}

void selectQueueFamilies(struct vkm_Device* vkm_device, const VkSurfaceKHR requestedSurface)
{
    // Query supported Queue Families
    uint32_t count_queueFamilyProperties = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(vkm_device->vk_physicalDevice, &count_queueFamilyProperties, NULL);
    VkQueueFamilyProperties* vk_queueFamilyProperties = (VkQueueFamilyProperties*)malloc(sizeof(VkQueueFamilyProperties)*count_queueFamilyProperties);
    vkGetPhysicalDeviceQueueFamilyProperties(vkm_device->vk_physicalDevice, &count_queueFamilyProperties, vk_queueFamilyProperties);

	vkm_device->queue_graphics.index = UINT32_MAX;
	vkm_device->queue_present.index  = UINT32_MAX;
	for (uint32_t i = 0; i < count_queueFamilyProperties; i++) 
	{
        VkBool32 supportsPresent = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(vkm_device->vk_physicalDevice, i, requestedSurface, &supportsPresent);

        if (vk_queueFamilyProperties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT && supportsPresent == VK_TRUE)
        {
            vkm_device->queue_graphics.index = i;
            vkm_device->queue_present.index = i;
            break;
        }
	}

    if (vkm_device->queue_graphics.index == UINT32_MAX || vkm_device->queue_present.index == UINT32_MAX)
    {
        perror("Separate Graphics and Presentation Queues is not supported!\n");
        exit(EXIT_FAILURE);
    }

    free(vk_queueFamilyProperties);
}

void createLogicalDevice(struct vkm_Device* vkm_device, const char** requestedExtensions, const uint32_t count_requestedExtensions)
{
    //*** Extension Validation

    // Query supported Device Extensions
    uint32_t count_supportedExtensions = 0;
    vkEnumerateDeviceExtensionProperties(vkm_device->vk_physicalDevice, NULL, &count_supportedExtensions, NULL);
    VkExtensionProperties* vk_supportedExtensions = (VkExtensionProperties*)malloc(sizeof(VkExtensionProperties)*count_supportedExtensions);
    vkEnumerateDeviceExtensionProperties(vkm_device->vk_physicalDevice, NULL, &count_supportedExtensions, vk_supportedExtensions);

    // Affirm that our requested Device Extensions are indeed avaliable
    for (size_t i = 0; i < count_requestedExtensions; ++i)
    {
        bool extensionSupported = false;
        for (size_t j = 0; j < count_supportedExtensions; ++j)
        {
            if (strcmp(requestedExtensions[i], vk_supportedExtensions[j].extensionName) == 0)
            {
                extensionSupported = true;
                break;
            }
        }
        if (!extensionSupported)
        {
            printf("Extension %s is not supported.\n", vk_supportedExtensions[i].extensionName);
            exit(EXIT_FAILURE);
        }
    }

    free(vk_supportedExtensions);

    //*** Queue Family Creation
    
    // Find and set state for Graphics Queue Family (Requied to be same as the present)
    float queuePriority = 1.f; 
    VkDeviceQueueCreateInfo ci_deviceQueue = {};
    ci_deviceQueue.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    ci_deviceQueue.queueFamilyIndex = vkm_device->queue_graphics.index;
    ci_deviceQueue.queueCount = 1;
    ci_deviceQueue.pQueuePriorities = &queuePriority;

    //*** Device Creation

    VkDeviceCreateInfo ci_device = {}; 
    ci_device.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    ci_device.queueCreateInfoCount = 1;
    ci_device.pQueueCreateInfos = &ci_deviceQueue;
    ci_device.enabledLayerCount = 0;
    ci_device.ppEnabledLayerNames = NULL;
    ci_device.enabledExtensionCount = count_requestedExtensions;
    ci_device.ppEnabledExtensionNames = requestedExtensions;
    ci_device.pEnabledFeatures = NULL;

    VkResult result = vkCreateDevice(vkm_device->vk_physicalDevice, &ci_device, NULL, &vkm_device->vk_device);
    if (result != VK_SUCCESS)
    {
        perror("Failed to create a Logical Device.\n");
        exit(EXIT_FAILURE);
    }

    vkGetDeviceQueue(vkm_device->vk_device, vkm_device->queue_graphics.index, 0, &vkm_device->queue_graphics.vk_handle);
    vkGetDeviceQueue(vkm_device->vk_device, vkm_device->queue_present.index, 0, &vkm_device->queue_present.vk_handle);
}

//*** Utility

uint32_t getMemoryTypeIndex(const struct vkm_Device* vkm_device, uint32_t typeBits, const VkMemoryPropertyFlags properties)
{
	// Iterate over all memory types available for the device used in this example
	for (uint32_t i = 0; i < vkm_device->vk_physicalDeviceMemoryProperties.memoryTypeCount; i++)
	{
		if ((typeBits & 1) == 1)
		{
			if ((vkm_device->vk_physicalDeviceMemoryProperties.memoryTypes[i].propertyFlags & properties) == properties)
			{
				return i;
			}
		}
		typeBits >>= 1;
	}

	perror("Could not find a suitable memory type!\n");
    exit(EXIT_FAILURE);
}