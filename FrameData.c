#include "FrameData.h"

#include <stdlib.h>
#include <stdio.h>

void init_vkm_FrameData(struct vkm_Device* vkm_device, struct vkm_FrameData* vkm_frameData)
{
    //*** Command Pool

    {
        VkCommandPoolCreateInfo ci_commandPool;
        ci_commandPool.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        ci_commandPool.pNext = NULL;
        ci_commandPool.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
        ci_commandPool.queueFamilyIndex = vkm_device->queue_graphics.index;

        VkResult result = vkCreateCommandPool(vkm_device->vk_device, &ci_commandPool, NULL, &vkm_frameData->vk_commandPool);
        if (result != VK_SUCCESS)
        {
            perror("Failed to allocate create command pool!\n");
            exit(EXIT_FAILURE);
        }
    }
    
    //*** Command Buffer

    {
        VkCommandBufferAllocateInfo ai_commandBuffer;
        ai_commandBuffer.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        ai_commandBuffer.pNext = NULL;
        ai_commandBuffer.commandPool = vkm_frameData->vk_commandPool;
        ai_commandBuffer.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        ai_commandBuffer.commandBufferCount = 1;

        VkResult result = vkAllocateCommandBuffers(vkm_device->vk_device, &ai_commandBuffer, &vkm_frameData->vk_commandBuffer);
        if (result != VK_SUCCESS)
        {
            perror("Failed to allocate command buffer!\n");
            exit(EXIT_FAILURE);
        }
    }

    //*** Semaphores

    {
        VkSemaphoreCreateInfo ci_imageAvailableSemaphore;
        ci_imageAvailableSemaphore.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        ci_imageAvailableSemaphore.pNext = NULL;
        ci_imageAvailableSemaphore.flags = 0x0;

        VkSemaphoreCreateInfo ci_renderingFinishedSemaphore;
        ci_renderingFinishedSemaphore.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        ci_renderingFinishedSemaphore.pNext = NULL;
        ci_renderingFinishedSemaphore.flags = 0x0;

        VkResult result = vkCreateSemaphore(vkm_device->vk_device, & ci_imageAvailableSemaphore, NULL, &vkm_frameData->vk_imageAvailableSemaphore);
        if (result != VK_SUCCESS)
        {
            perror("Failed to create image available sempahore!\n");
            exit(EXIT_FAILURE);
        }
        result = vkCreateSemaphore(vkm_device->vk_device, & ci_renderingFinishedSemaphore, NULL, &vkm_frameData->vk_renderingFinishedSemaphore);
        if (result != VK_SUCCESS)
        {
            perror("Failed to create rendering finished sempahore!\n");
            exit(EXIT_FAILURE);
        }
    }

    //*** Fence 

    {
        VkFenceCreateInfo ci_fence;
        ci_fence.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        ci_fence.pNext = NULL;
        ci_fence.flags = VK_FENCE_CREATE_SIGNALED_BIT; // Fence is created in a signaled state

        VkResult result = vkCreateFence(vkm_device->vk_device, &ci_fence, NULL, &vkm_frameData->vk_fence);
        if (result != VK_SUCCESS)
        {
            perror("Failed to create a Fence!\n");
            exit(EXIT_FAILURE);
        }
    }

}
