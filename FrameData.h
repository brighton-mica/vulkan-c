#ifndef FRAME_DATA_H
#define FRAME_DATA_H

/*

We considered the following when deciding what data appropriately belongs in the 
FrameData structure.

* Command Pools

   "Command pools are externally synchronized, meaning that a command pool must not
    be used concurrently in multiple threads." - VkSpec

    Given this, we decide to have a unique command pool per frame (where each frame
    is running on a separate thread).

    * Command Buffer Recycling

        Three methods are described in this artcle (https://github.com/KhronosGroup/Vulkan-Samples/blob/master/samples/performance/command_buffer_usage/command_buffer_usage_tutorial.md).
        Using vkResetCommandPool to reset all command buffers allocated in the pool
        seems to have the best performance among the other strategies. Having one
        command pool per frame will allow us reuse the memory allocated for command
        buffer with a lower CPU overhead. If we are doing this method, the article 
        recommends not using the RESET_COMMAND_BUFFER_BIT flag.

        https://developer.nvidia.com/blog/vulkan-dos-donts/
        https://zeux.io/2020/02/27/writing-an-efficient-vulkan-renderer/

        Both Nvidia and Zuex (articles above) recommend using at lead L * T command
        pools (L = # of buffered frames, T = # of threads recording commands). We are
        going to start out with L command pools. 
        TODO - multiple threads per frame for recording

    Other Resources

    "GDC 2016: High-performance, Low-Overhead Rendering with OpenGL and Vulkan"


* Descriptor Pools

   "Descriptor pools are externally synchronized, meaning that the application must 
    not allocate and/or free descriptor sets from the same pool in multiple threads
    simultaneously." - VkSpec

    "GDC 2016: High-performance, Low-Overhead Rendering with OpenGL and Vulkan"

    The speaker above video) recommends creating Descriptor Pools based on lifespan.
    Reseting a Descriptor pool for a given game level (frame, material, draw) is a 
    quick, clean way to delete lots os descriptor sets.
    Descriptor Pools should be created based on lifespan. This allows for a lean

*/

#include <vulkan/vulkan.h>
#include "Device.h"

struct vkm_FrameData
{
    VkCommandPool   vk_commandPool;
    VkCommandBuffer vk_commandBuffer;

    VkDescriptorPool vk_descriptorPool;

    VkFramebuffer    vk_frameBuffer;

    VkSemaphore vk_imageAvailableSemaphore;
    VkSemaphore vk_renderingFinishedSemaphore;

    VkFence vk_fence;
};


void init_vkm_FrameData(struct vkm_Device* vkm_device, struct vkm_FrameData* vkm_frameData);


void record();
    // vkBeginCommandBuffer();
    // vkCmdBeginRenderPass();

    // vkCmdBindPipeline();
        // vkCmdPushConstants();
        // vkCmdBindDescriptorSets();

        // draw();

    // vkCmdEndRenderPass();
    // vkEndCommandBuffer();

#endif // FRAME_DATA_H