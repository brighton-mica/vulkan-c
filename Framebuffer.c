#include "Framebuffer.h"

#include <stdlib.h>
#include <stdio.h>


void createDepthImageView(struct vkm_Device* vkm_device, struct vkm_Swapchain* vkm_swapchain, struct vkm_RenderPass* vkm_renderPass, struct vkm_FramebufferManager* vkm_framebufferManager)
{
    VkImageCreateInfo ci_image;
	ci_image.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    ci_image.pNext = NULL;
    ci_image.flags = 0x0;
	ci_image.imageType = VK_IMAGE_TYPE_2D;
	ci_image.format = vkm_renderPass->ci_renderPass.pAttachments[1].format;
	ci_image.extent.width = vkm_swapchain->ci_swapchain.imageExtent.width;
	ci_image.extent.height = vkm_swapchain->ci_swapchain.imageExtent.height;
    ci_image.extent.depth = 1;
	ci_image.mipLevels = 1;
	ci_image.arrayLayers = 1;
	ci_image.samples = VK_SAMPLE_COUNT_1_BIT;
	ci_image.tiling = VK_IMAGE_TILING_OPTIMAL;
	ci_image.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
    ci_image.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    ci_image.queueFamilyIndexCount = 0;
    ci_image.pQueueFamilyIndices = NULL;
	ci_image.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

	VkResult result = vkCreateImage(vkm_device->vk_device, &ci_image, NULL, &vkm_framebufferManager->vkm_depthStencilAttachment.vk_depthImage);
    if (result != VK_SUCCESS)
    {
        perror("Failed to create VkImage for depth attachemnt!\n");
        exit(EXIT_FAILURE);
    }

	// Allocate memory for the image (device local) and bind it to our image
	VkMemoryAllocateInfo vk_memAlloc;
	vk_memAlloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    vk_memAlloc.pNext = NULL;

	VkMemoryRequirements vk_memReqs;
	vkGetImageMemoryRequirements(vkm_device->vk_device, vkm_framebufferManager->vkm_depthStencilAttachment.vk_depthImage, &vk_memReqs);
	vk_memAlloc.allocationSize = vk_memReqs.size;
	vk_memAlloc.memoryTypeIndex = getMemoryTypeIndex(vkm_device, vk_memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	result = vkAllocateMemory(vkm_device->vk_device, &vk_memAlloc, NULL, &vkm_framebufferManager->vkm_depthStencilAttachment.vk_deviceMemory);
    if (result != VK_SUCCESS)
    {
        perror("Failed to allocate memory for Depth/Stencil Image!\n");
        exit(EXIT_FAILURE);
    }
	result = vkBindImageMemory(vkm_device->vk_device, vkm_framebufferManager->vkm_depthStencilAttachment.vk_depthImage, vkm_framebufferManager->vkm_depthStencilAttachment.vk_deviceMemory, 0);
    if (result != VK_SUCCESS)
    {
        perror("Failed to bind Depth/Stencil Image to memory!\n");
        exit(EXIT_FAILURE);
    }

	// Create a view for the depth stencil image
	// Images aren't directly accessed in Vulkan, but rather through views described by a subresource range
	// This allows for multiple views of one image with differing ranges (e.g. for different layers)
	VkImageViewCreateInfo ci_depthStencilView = {};
	ci_depthStencilView.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    ci_depthStencilView.pNext = NULL;
    ci_depthStencilView.flags = 0x0;
	ci_depthStencilView.image = vkm_framebufferManager->vkm_depthStencilAttachment.vk_depthImage;
	ci_depthStencilView.viewType = VK_IMAGE_VIEW_TYPE_2D;
	ci_depthStencilView.format = ci_image.format;
    ci_depthStencilView.components.r = VK_COMPONENT_SWIZZLE_R;
    ci_depthStencilView.components.g = VK_COMPONENT_SWIZZLE_G;
    ci_depthStencilView.components.b = VK_COMPONENT_SWIZZLE_B;
    ci_depthStencilView.components.a = VK_COMPONENT_SWIZZLE_A;
	ci_depthStencilView.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
	ci_depthStencilView.subresourceRange.baseMipLevel = 0;
	ci_depthStencilView.subresourceRange.levelCount = 1;
	ci_depthStencilView.subresourceRange.baseArrayLayer = 0;
	ci_depthStencilView.subresourceRange.layerCount = 1;

    result = vkCreateImageView(vkm_device->vk_device, &ci_depthStencilView, NULL, &vkm_framebufferManager->vkm_depthStencilAttachment.vk_depthImageView);
    if (result != VK_SUCCESS)
    {
        perror("Failed to create ImageView for Depth/Stencil attachment!\n");
        exit(EXIT_FAILURE);
    }
}

void createFramebuffers(struct vkm_Device* vkm_device, struct vkm_Swapchain* vkm_swapchain, struct vkm_RenderPass* vkm_renderPass, struct vkm_FramebufferManager* vkm_framebufferManager)
{
    for (size_t i = 0; i < COUNT_SWAPCHAIN_IMAGES; ++i)
    {
        VkImageView attachments[2];
        attachments[0] = vkm_swapchain->vk_imageViews[i];
        attachments[1] = vkm_framebufferManager->vkm_depthStencilAttachment.vk_depthImageView;

        VkFramebufferCreateInfo ci_frameBuffer = { };
        ci_frameBuffer.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        ci_frameBuffer.renderPass = vkm_renderPass->vk_renderPass;
        ci_frameBuffer.attachmentCount = 2;
        ci_frameBuffer.pAttachments = attachments;
        ci_frameBuffer.width = vkm_swapchain->ci_swapchain.imageExtent.width;
        ci_frameBuffer.height = vkm_swapchain->ci_swapchain.imageExtent.height;
        ci_frameBuffer.layers = 1;
        
        VkResult result = vkCreateFramebuffer(vkm_device->vk_device, &ci_frameBuffer, NULL, &vkm_framebufferManager->vk_framebuffers[i]);
        if (result != VK_SUCCESS)
        {
            perror("Failed to create Framebuffer!\n");
            exit(EXIT_FAILURE);
        }
    }
}
