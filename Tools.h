#include <vulkan/vulkan.h>

const char* toStr_VkFormat(const VkFormat vk_format)
{
    switch (vk_format)
    {
        case VK_FORMAT_R8G8B8A8_SRGB:
            return "VK_FORMAT_R8G8B8A8_SRGB";
        case VK_FORMAT_B8G8R8A8_UNORM:
            return "VK_FORMAT_B8G8R8A8_UNORM";

        default:
            return "UNKNOWN FORMAT";
    }
}

const char* toStr_VkColorSpaceKHR(const VkColorSpaceKHR vk_colorSpace)
{
    switch (vk_colorSpace)
    {
        case VK_COLOR_SPACE_SRGB_NONLINEAR_KHR:
            return "VK_COLOR_SPACE_SRGB_NONLINEAR_KHR";

        default:
            return "UNKNOWN COLOR SPACE";
    }
}
