#ifndef CUBE_H
#define CUBE_H

#include <vulkan/vulkan.h>

const VkDeviceSize nbytes_cubeVertexBindingData = sizeof(float) * 216; 
const float cubeVertexBindingData[216] = {

    // front face
    -0.5f,  0.5f, -0.5f,    0.f, 0.f, -1.f, 
     0.5f,  0.5f, -0.5f,    0.f, 0.f, -1.f, 
     0.5f, -0.5f, -0.5f,    0.f, 0.f, -1.f, 
    -0.5f,  0.5f, -0.5f,    0.f, 0.f, -1.f, 
     0.5f, -0.5f, -0.5f,    0.f, 0.f, -1.f, 
    -0.5f, -0.5f, -0.5f,    0.f, 0.f, -1.f, 

    // right face
     0.5f,  0.5f, -0.5f,    1.f, 0.f, 0.f,
     0.5f,  0.5f,  0.5f,    1.f, 0.f, 0.f,
     0.5f, -0.5f,  0.5f,    1.f, 0.f, 0.f,
     0.5f,  0.5f, -0.5f,    1.f, 0.f, 0.f,
     0.5f, -0.5f,  0.5f,    1.f, 0.f, 0.f,
     0.5f, -0.5f, -0.5f,    1.f, 0.f, 0.f,

    // back face
     0.5f,  0.5f,  0.5f,    0.f, 0.f, 1.f,
    -0.5f,  0.5f,  0.5f,    0.f, 0.f, 1.f,
    -0.5f, -0.5f,  0.5f,    0.f, 0.f, 1.f,
     0.5f,  0.5f,  0.5f,    0.f, 0.f, 1.f,
    -0.5f, -0.5f,  0.5f,    0.f, 0.f, 1.f,
     0.5f, -0.5f,  0.5f,    0.f, 0.f, 1.f,

    // left face
    -0.5f,  0.5f,  0.5f,    1.f, 0.f, 0.f,
    -0.5f,  0.5f, -0.5f,    1.f, 0.f, 0.f,
    -0.5f, -0.5f, -0.5f,    1.f, 0.f, 0.f,
    -0.5f,  0.5f,  0.5f,    1.f, 0.f, 0.f,
    -0.5f, -0.5f, -0.5f,    1.f, 0.f, 0.f,
    -0.5f, -0.5f,  0.5f,    1.f, 0.f, 0.f,
                        
    // top face
    -0.5f, -0.5f, -0.5f,    0.f, -1.f, 0.f,
     0.5f, -0.5f, -0.5f,    0.f, -1.f, 0.f,
     0.5f, -0.5f,  0.5f,    0.f, -1.f, 0.f,
    -0.5f, -0.5f, -0.5f,    0.f, -1.f, 0.f,
     0.5f, -0.5f,  0.5f,    0.f, -1.f, 0.f,
    -0.5f, -0.5f,  0.5f,    1.f, -1.f, 0.f,

    // bottom face
    -0.5f,  0.5f,  0.5f,    0.f, 1.f, 0.f,
     0.5f,  0.5f,  0.5f,    0.f, 1.f, 0.f,
     0.5f,  0.5f, -0.5f,    0.f, 1.f, 0.f,
    -0.5f,  0.5f,  0.5f,    0.f, 1.f, 0.f,
     0.5f,  0.5f, -0.5f,    0.f, 1.f, 0.f,
    -0.5f,  0.5f, -0.5f,    0.f, 1.f, 0.f,
};

#endif // CUBE_H