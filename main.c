#include "Instance.h"
#include "Device.h"
#include "Swapchain.h"
#include "RenderPass.h"
#include "Framebuffer.h"
#include "FrameData.h"
#include "Pipeline.h"

#include "Cube.h"


#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>

#include "vk_mem_alloc.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>

#define WIN_X 600
#define WIN_Y 600

#define COUNT_FRAMES_IN_FLIGHT 2

struct vkm_Buffer
{
    VkBuffer vk_buffer;
    VmaAllocation vma_allocation;
};

void initVertexData(struct vkm_Device* vkm_device, VmaAllocator* vma_allocator, VkCommandPool vk_commandPool, struct vkm_Buffer* vkm_deviceLocalBuffer)
{
    struct vkm_Buffer vkm_hostVisibleBuffer = { 0 };

    //*** Host Visible Buffer

    VkBufferCreateInfo ci_buffer = { 0 };
    ci_buffer.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    ci_buffer.size = nbytes_cubeVertexBindingData;
    ci_buffer.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
    ci_buffer.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    VmaAllocationCreateInfo vma_allocationCreateInfo = { 0 };
    vma_allocationCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;
    vma_allocationCreateInfo.usage = VMA_MEMORY_USAGE_CPU_ONLY;

    VmaAllocationInfo vma_hostVisibleAllocationInfo = { 0 };
    VkResult result = vmaCreateBuffer(*vma_allocator, &ci_buffer, &vma_allocationCreateInfo, &vkm_hostVisibleBuffer.vk_buffer, &vkm_hostVisibleBuffer.vma_allocation, &vma_hostVisibleAllocationInfo);
    if (result != VK_SUCCESS)
    {
        perror("Failed to create buffer!\n");
        exit(EXIT_FAILURE);
    }

    memcpy(vma_hostVisibleAllocationInfo.pMappedData, cubeVertexBindingData, nbytes_cubeVertexBindingData);

    //*** Device Local Buffer

    ci_buffer.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
    vma_allocationCreateInfo.flags = 0x0;
    vma_allocationCreateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

    result = vmaCreateBuffer(*vma_allocator, &ci_buffer, &vma_allocationCreateInfo, &vkm_deviceLocalBuffer->vk_buffer, &vkm_deviceLocalBuffer->vma_allocation, NULL);
    if (result != VK_SUCCESS)
    {
        perror("Failed to create buffer!\n");
        exit(EXIT_FAILURE);
    }

    //*** Copying of GPU Host Visible tp GPU Device Local

    VkCommandBuffer vk_commandBuffer;

    VkCommandBufferAllocateInfo ai_commandBuffer = { 0 };
    ai_commandBuffer.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    ai_commandBuffer.commandPool = vk_commandPool;
    ai_commandBuffer.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    ai_commandBuffer.commandBufferCount = 1;

    result = vkAllocateCommandBuffers(
        vkm_device->vk_device, 
        &ai_commandBuffer, 
        &vk_commandBuffer);
    assert(result == VK_SUCCESS && "Staging Buffer - vkAllocateCommandBuffers() failed!\n");

    VkCommandBufferBeginInfo cmdBufferBeginInfo = { 0 };
    cmdBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    result = vkBeginCommandBuffer(vk_commandBuffer, &cmdBufferBeginInfo);
    assert(result == VK_SUCCESS && "Staging Buffer - vkBeginCommandBuffer() failed!\n");

        VkBufferCopy copyRegion = { 0 };

        copyRegion.size = nbytes_cubeVertexBindingData;
        vkCmdCopyBuffer(vk_commandBuffer, vkm_hostVisibleBuffer.vk_buffer, vkm_deviceLocalBuffer->vk_buffer, 1, &copyRegion);

    result = vkEndCommandBuffer(vk_commandBuffer);
    assert(result == VK_SUCCESS && "Staging Buffer - vkEndCommandBuffer() failed!\n");

    //* Submit Command Buffer immediatley
    VkSubmitInfo vk_submitInfo = { 0 };
    vk_submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    vk_submitInfo.commandBufferCount = 1;
    vk_submitInfo.pCommandBuffers = &vk_commandBuffer;

    VkFenceCreateInfo ci_fence = { 0 };
    ci_fence.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

    VkFence vk_fence;
    result = vkCreateFence(vkm_device->vk_device, &ci_fence, NULL, &vk_fence);
    assert(result == VK_SUCCESS && "Staging Buffer - vkCreateFence() failed!\n");

    result = vkQueueSubmit(vkm_device->queue_graphics.vk_handle, 1, &vk_submitInfo, vk_fence);
    assert(result == VK_SUCCESS && "Staging Buffer - vkQueueSubmit() failed!\n");

    result = vkWaitForFences(vkm_device->vk_device, 1, &vk_fence, VK_TRUE, UINT64_MAX);
    assert(result == VK_SUCCESS && "Staging Buffer - vkWaitForFences() failed!\n");

    // ----------------------------
    // Cleanup
    // ----------------------------
    vkDestroyFence(vkm_device->vk_device, vk_fence, NULL);
    vkFreeCommandBuffers(vkm_device->vk_device, vk_commandPool, 1, &vk_commandBuffer);

    vmaDestroyBuffer(*vma_allocator, vkm_hostVisibleBuffer.vk_buffer, vkm_hostVisibleBuffer.vma_allocation);
}


int main()
{
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    GLFWwindow* glfw_window = glfwCreateWindow(WIN_X, WIN_Y, "C - Vulkan", NULL, NULL);

    //*** Instance 

    uint32_t count_instanceExtensions = 0;
    const char** instanceExtensions = glfwGetRequiredInstanceExtensions(&count_instanceExtensions);

    const uint32_t count_instanceLayers = 1;
    const char* instanceLayers[1] = { "VK_LAYER_KHRONOS_validation" };
    struct vkm_Instance vkm_instance;
    createInstance(&vkm_instance, instanceExtensions, count_instanceExtensions, instanceLayers, count_instanceLayers);

    //*** Surface

    VkSurfaceKHR requestedSurface;
    VkResult res = glfwCreateWindowSurface(vkm_instance.vk_instance, glfw_window, NULL, &requestedSurface);
    if (res != VK_SUCCESS) 
    {
        perror("Failed to create VkSurfaceKHR object!\n");
        exit(EXIT_FAILURE);
    }

    //*** Device 
 
    // TODO - fix INTEL-MESA warning
    struct vkm_Device vkm_device;
    selectPhysicalDevice(&vkm_device, &vkm_instance);

    selectQueueFamilies(&vkm_device, requestedSurface);

    const uint32_t count_deviceExtensions = 1;
    const char* deviceExtensions[1] = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };
    createLogicalDevice(&vkm_device, deviceExtensions, count_deviceExtensions);

    //*** VMA

    VmaAllocatorCreateInfo vma_allocatorCreateInfo = { 0 };
    vma_allocatorCreateInfo.vulkanApiVersion = VK_API_VERSION_1_2;
    vma_allocatorCreateInfo.physicalDevice = vkm_device.vk_physicalDevice;
    vma_allocatorCreateInfo.device = vkm_device.vk_device;
    vma_allocatorCreateInfo.instance = vkm_instance.vk_instance;

    VmaAllocator vma_allocator = { 0 };
    vmaCreateAllocator(&vma_allocatorCreateInfo, &vma_allocator);

    //*** Swapchain

    VkFormat     requestedFormat = VK_FORMAT_B8G8R8A8_UNORM; // VK_FORMAT_R8G8B8A8_UNORM;
    uint32_t     requestedImageCount = 2;
    VkExtent2D   requestedImageExtent = { WIN_X, WIN_Y };

    struct vkm_Swapchain vkm_swapchain;
    createSwapchain(&vkm_device, &vkm_swapchain, requestedImageCount, requestedSurface, requestedFormat, requestedImageExtent);

    //*** RenderPass

    struct vkm_RenderPass vkm_renderPass;
    createRenderPass(&vkm_device, &vkm_swapchain, &vkm_renderPass);

    //*** Framebuffer(s)

    struct vkm_FramebufferManager vkm_framebufferManager;
    createDepthImageView(&vkm_device, &vkm_swapchain, &vkm_renderPass, &vkm_framebufferManager);
    createFramebuffers(&vkm_device, &vkm_swapchain, &vkm_renderPass, &vkm_framebufferManager);

    //*** Frame Data

    uint32_t frameIndex = 0;
    struct vkm_FrameData vkm_frameData[COUNT_FRAMES_IN_FLIGHT];
    for ( size_t i = 0; i < COUNT_FRAMES_IN_FLIGHT; ++i)
    {
        init_vkm_FrameData(&vkm_device, &vkm_frameData[i]);
    }

    //*** Pipeline

    struct vkm_GraphicsPipeline vkm_graphicsPipeline;
    createDefaultGraphicsPipeline(&vkm_device, &vkm_swapchain, &vkm_renderPass, &vkm_graphicsPipeline);

    printf("Vulkan Init Complete!\n");

    //*** Vertex Data Init

    struct vkm_Buffer vkm_triangleBuffer = { 0 };
    initVertexData(&vkm_device, &vma_allocator, vkm_frameData[0].vk_commandPool, &vkm_triangleBuffer);

    VkClearValue vk_clearValues[2] = { 0 };
    vk_clearValues[0].color = (VkClearColorValue) { 0.22f, 0.22f, 0.22f, 1.0f };
    vk_clearValues[1].depthStencil = (VkClearDepthStencilValue) { 1.0f, 0 };

    VkDeviceSize vk_vertexBufferOffsets = { 0 };

    while(!glfwWindowShouldClose(glfw_window))
    {
        glfwPollEvents();

        VkResult result = vkWaitForFences(vkm_device.vk_device, 1, &vkm_frameData[frameIndex].vk_fence, VK_FALSE, UINT64_MAX);
        assert(result == VK_SUCCESS && "vkWaitForFences() failure!\n");

        result = vkResetFences(vkm_device.vk_device, 1, &vkm_frameData[frameIndex].vk_fence);
        assert(result == VK_SUCCESS && "vkResetFences() failure!\n");

        result = vkAcquireNextImageKHR(vkm_device.vk_device, vkm_swapchain.vk_swapchain, UINT64_MAX, vkm_frameData[frameIndex].vk_imageAvailableSemaphore, VK_NULL_HANDLE, &vkm_swapchain.imageIndex);
        assert(result == VK_SUCCESS && "vkAcquireNextImageKHR failure!\n");
        // printf("Swapchain Image Index: %u\tFrame Index: %u\n", vk_swapchain.imageIndex, frameIndex);

        //*** Command Buffer Recording
        {
            VkCommandBufferBeginInfo bi_commandBuffer;
            bi_commandBuffer.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            bi_commandBuffer.pNext = NULL;
            bi_commandBuffer.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
            bi_commandBuffer.pInheritanceInfo = NULL;

            VkRenderPassBeginInfo bi_renderPass;
            bi_renderPass.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            bi_renderPass.pNext = NULL;
            bi_renderPass.renderPass = vkm_renderPass.vk_renderPass;
            bi_renderPass.framebuffer = vkm_framebufferManager.vk_framebuffers[vkm_swapchain.imageIndex];
            bi_renderPass.renderArea.extent = vkm_swapchain.ci_swapchain.imageExtent;
            bi_renderPass.renderArea.offset = (VkOffset2D) { .x = 0, .y  = 0 };
            bi_renderPass.clearValueCount = 2;
            bi_renderPass.pClearValues = vk_clearValues;

            result = vkBeginCommandBuffer(vkm_frameData[frameIndex].vk_commandBuffer, &bi_commandBuffer);
            assert(result == VK_SUCCESS && "vkBeginCommandBufferFailed!\n");

            vkCmdBeginRenderPass(vkm_frameData[frameIndex].vk_commandBuffer, &bi_renderPass, VK_SUBPASS_CONTENTS_INLINE);

            {
                vkCmdBindPipeline(vkm_frameData[frameIndex].vk_commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, vkm_graphicsPipeline.vk_pipeline);

                vkCmdBindVertexBuffers(vkm_frameData[frameIndex].vk_commandBuffer, 0, 1, &vkm_triangleBuffer.vk_buffer, &vk_vertexBufferOffsets);

                vkCmdDraw(vkm_frameData[frameIndex].vk_commandBuffer, 216, 1, 0, 0);
            }

            vkCmdEndRenderPass(vkm_frameData[frameIndex].vk_commandBuffer);

            result = vkEndCommandBuffer(vkm_frameData[frameIndex].vk_commandBuffer);
            assert(result == VK_SUCCESS && "vkEndCommandBufferFailed!\n");
        }

        const VkPipelineStageFlags waitStageMask[1] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
        VkSubmitInfo vk_submitInfo;
        vk_submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        vk_submitInfo.pNext = NULL;
        vk_submitInfo.waitSemaphoreCount = 1;
        vk_submitInfo.pWaitSemaphores = &vkm_frameData[frameIndex].vk_imageAvailableSemaphore;
        vk_submitInfo.pWaitDstStageMask = waitStageMask;
        vk_submitInfo.commandBufferCount = 1;
        vk_submitInfo.pCommandBuffers = &vkm_frameData[frameIndex].vk_commandBuffer;
        vk_submitInfo.signalSemaphoreCount = 1;
        vk_submitInfo.pSignalSemaphores = &vkm_frameData[frameIndex].vk_renderingFinishedSemaphore;
        
        result = vkQueueSubmit(vkm_device.queue_graphics.vk_handle, 1, &vk_submitInfo, vkm_frameData[frameIndex].vk_fence);
        assert(result == VK_SUCCESS && "Queue Submission Failed!");

        VkPresentInfoKHR vk_prensentInfoKHR;
        vk_prensentInfoKHR.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        vk_prensentInfoKHR.pNext = NULL;
        vk_prensentInfoKHR.waitSemaphoreCount = 1;
        vk_prensentInfoKHR.pWaitSemaphores = &vkm_frameData[frameIndex].vk_renderingFinishedSemaphore;
        vk_prensentInfoKHR.swapchainCount = 1;
        vk_prensentInfoKHR.pSwapchains = &vkm_swapchain.vk_swapchain;
        vk_prensentInfoKHR.pImageIndices = &vkm_swapchain.imageIndex;
        vk_prensentInfoKHR.pResults = NULL;

        result = vkQueuePresentKHR(vkm_device.queue_present.vk_handle, &vk_prensentInfoKHR);
        assert(result == VK_SUCCESS && "Presentation failed or suboptimal!\n");

        frameIndex = (frameIndex + 1) % 2;
    }

    vkDeviceWaitIdle(vkm_device.vk_device);

    vmaDestroyBuffer(vma_allocator, vkm_triangleBuffer.vk_buffer, vkm_triangleBuffer.vma_allocation);
    vkm_triangleBuffer.vk_buffer = VK_NULL_HANDLE;
    vmaDestroyAllocator(vma_allocator);

    {
        for (size_t i = 0; i < COUNT_FRAMES_IN_FLIGHT; ++i)
        {
            vkDestroyCommandPool(vkm_device.vk_device, vkm_frameData[i].vk_commandPool, NULL);
            vkDestroySemaphore(vkm_device.vk_device, vkm_frameData[i].vk_imageAvailableSemaphore, NULL);
            vkDestroySemaphore(vkm_device.vk_device, vkm_frameData[i].vk_renderingFinishedSemaphore, NULL);
            vkDestroyFence(vkm_device.vk_device, vkm_frameData[i].vk_fence, NULL);
        }

        vkDestroyImage(vkm_device.vk_device, vkm_framebufferManager.vkm_depthStencilAttachment.vk_depthImage, NULL);
        vkDestroyImageView(vkm_device.vk_device, vkm_framebufferManager.vkm_depthStencilAttachment.vk_depthImageView, NULL);
        vkFreeMemory(vkm_device.vk_device, vkm_framebufferManager.vkm_depthStencilAttachment.vk_deviceMemory, NULL);

        for (size_t i = 0; i < COUNT_SWAPCHAIN_IMAGES; ++i)
        {
            vkDestroyFramebuffer(vkm_device.vk_device, vkm_framebufferManager.vk_framebuffers[i], NULL);
            vkDestroyImageView(vkm_device.vk_device, vkm_swapchain.vk_imageViews[i], NULL);
        }

        vkDestroyPipeline(vkm_device.vk_device, vkm_graphicsPipeline.vk_pipeline, NULL);
        vkDestroyPipelineLayout(vkm_device.vk_device, vkm_graphicsPipeline.vk_pipelineLayout, NULL);
        vkDestroyRenderPass(vkm_device.vk_device, vkm_renderPass.vk_renderPass, NULL);
        vkDestroySwapchainKHR(vkm_device.vk_device, vkm_swapchain.vk_swapchain, NULL);
        vkDestroyDevice(vkm_device.vk_device, NULL);
        vkDestroySurfaceKHR(vkm_instance.vk_instance, vkm_swapchain.ci_swapchain.surface, NULL);
        vkDestroyInstance(vkm_instance.vk_instance, NULL);

        glfwDestroyWindow(glfw_window);
        glfwTerminate();
    }

    return 0;
}