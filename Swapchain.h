#ifndef SWAPCHAIN_H
#define SWAPCHAIN_H

#include "Device.h"

#define COUNT_SWAPCHAIN_IMAGES 2

#include <vulkan/vulkan.h>

struct vkm_Swapchain
{
    VkSwapchainKHR vk_swapchain;
    VkSwapchainCreateInfoKHR ci_swapchain;

    VkImage     vk_images[COUNT_SWAPCHAIN_IMAGES];
    VkImageView vk_imageViews[COUNT_SWAPCHAIN_IMAGES];
    uint32_t    imageIndex;;
};

void createSwapchain(
    const struct vkm_Device* vkm_device,
    struct vkm_Swapchain* vk_swapchain,
    const uint32_t requestedImageCount,
    const VkSurfaceKHR requestedSurface, 
    const VkFormat requestedFormat,
    const VkExtent2D requestedImageExtent);

#endif // SWAPCHAIN_H