#ifndef PIPELINE_H
#define PIPELINE_H

#include "Device.h"
#include "Swapchain.h"
#include "RenderPass.h"

#include <vulkan/vulkan.h>

struct vkm_GraphicsPipeline
{
    VkPipeline vk_pipeline;
    VkPipelineLayout vk_pipelineLayout;
};

void createDefaultGraphicsPipeline(struct vkm_Device* vkm_device, struct vkm_Swapchain* vk_swapchain, struct vkm_RenderPass* vkm_renderPass, struct vkm_GraphicsPipeline* vkm_graphicsPipeline);


#endif // PIPELINE_H